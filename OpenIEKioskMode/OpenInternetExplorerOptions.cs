﻿using System.Linq;

namespace OpenIEKioskMode
{
    public class OpenInternetExplorerOptions
    {
        public int ScreenNo { get; set; }
        public string Url { get; set; }

        public OpenInternetExplorerOptions(string[] args)
        {
            ScreenNo = int.Parse(args[0]) - 1;
            Url = string.Join("", args.Skip(1));
        }

    }
}
