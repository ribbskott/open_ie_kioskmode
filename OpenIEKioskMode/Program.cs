﻿using System;

namespace OpenIEKioskMode
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args.Length < 2)
            {
                Console.WriteLine("You need to specify screen and URL. e.g OpenIEKioskMode 1 'http://www.google.se'. This will open IE on the primary screen with Google.");
                return;
            }

            try
            {
                var options = new OpenInternetExplorerOptions(args);
                var ieOpener = new InternetExplorerOpener(options);
                ieOpener.OpenInternetExplorer();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }



        }
        
    }


}
