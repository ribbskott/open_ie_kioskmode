﻿using System;
using System.Windows.Forms;

namespace OpenIEKioskMode
{
    public class InternetExplorerOpener
    {
        private readonly OpenInternetExplorerOptions _options;

        public InternetExplorerOpener(OpenInternetExplorerOptions options)
        {
            _options = options;
        }

        public void OpenInternetExplorer()
        {
            var screenInfo = new ScreenInfo();
            var screen = screenInfo.GetScreenOfIndex(_options.ScreenNo);
            if (screen == null)
            {
                Console.WriteLine("Could not find screen at the given index. Exiting application.");
                return;
            }

            var ieInstance = CreateIeInstance();
            ConfigureIe(ieInstance, screen);

        }

        private static dynamic CreateIeInstance()

        {
            var ieType = Type.GetTypeFromProgID("InternetExplorer.Application");
            dynamic ieInstance = Activator.CreateInstance(ieType);
            return ieInstance;
        }

        private void ConfigureIe(dynamic ieInstance, Screen screen)
        {

            var hwnd = ieInstance.hwnd;
            ieInstance.Width = screen.Bounds.Width;
            ieInstance.Height = screen.Bounds.Height;
            ieInstance.Left = screen.Bounds.Left;
            ieInstance.ToolBar = 0;
            ieInstance.StatusBar = false;
            ieInstance.Resizable = false;

            ieInstance.Navigate2(_options.Url);

            var winApiWrapper = new WinApiWrapper();
            winApiWrapper.MaximizeWindow(hwnd);
            winApiWrapper.StripTitleBar(hwnd);

        }

    }
}
