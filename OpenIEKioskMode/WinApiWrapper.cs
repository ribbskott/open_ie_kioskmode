﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenIEKioskMode
{
    public class WinApiWrapper
    {
        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, IntPtr nIndex, int dwNewLong);

        private const int SwShowmaximized = 3;
        private const int GwlStyle = -16;

        public void MaximizeWindow(int windowHandle)
        {
            SetWindowLong((IntPtr)windowHandle, (IntPtr)GwlStyle, 0);
        }

        public void StripTitleBar(int windowHandle)
        {
            ShowWindowAsync((IntPtr)windowHandle, SwShowmaximized);
        }
    }
}
