﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenIEKioskMode
{
    public class ScreenInfo
    {
        public int GetNoOfScreens()
        {
            return Screen.AllScreens.Length;
        }
        
        public Screen GetScreenOfIndex(int index)
        {
            return index > Screen.AllScreens.Length ? null : Screen.AllScreens[index];
        }
    }
}
